package main

import (
	"fmt"
	"log"
	"os"
	"syscall"
	"time"
)

func main() {
	//This takes a little bit longer to mount, so we do it after the others
	max_attempts := 5
	delay := 1 * time.Second
	timeout := 5 * time.Second

	timeoutCh := time.After(timeout)

	attempt := 0
	ssdMounted := false
	for {
		select {
		case <-timeoutCh:
			log.Printf("Timed out waiting for /dev/sda1 to mount\n")
			break
		default:
			attempt++

			fmt.Printf("Waiting for /dev/sda1 to mount\n")
			//SSD storage via usb
			if err := syscall.Mount("/dev/sda1", "/ssd", "ext4", 0, ""); err != nil {
				//log.Printf("Could not mount permanent storage partition /dev/sda1 as ext4: %v", err)
				time.Sleep(delay)
				continue
			}
			ssdMounted = true
			log.Printf("Mounted /dev/sda1 as /ssd\n")

		}
		if attempt > max_attempts {
			log.Printf("Giving up waiting for /dev/sda1 to mount\n")
			break
		}
		if ssdMounted {
			break
		}
	}

	os.Exit(125)
}
