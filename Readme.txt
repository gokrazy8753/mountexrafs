In config.json under PackageConfig,

        "mountextrafs": {
            "ExtraFilePaths": {
                "/ssd": ""
            }
        }

the /ssd ensures gokrazy creates /ssd directory in squashfs for us to mount /dev/sda1
